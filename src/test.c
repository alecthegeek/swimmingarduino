#include <stdio.h>
#include <stdlib.h>


typedef struct { const int a; const int b; const int c;
  const int const d[];
	} x_t;

static const x_t const x[] =
	{{ 1, 2, 3,
			{4,5,6,7,}},
         { 11, 12, 13,
			(const int[]){14,15,}},
         { 21, 22, 23,
			(const int[]){24,25,26,}},
	};


int main(int argc, char **argv)
{
  for (int j=0 ; j < sizeof(x)/sizeof(x_t); j++)
    {
	printf("a is %u, b is %u, c is %u\n",x[j].a,x[j].b,x[j].c);


        printf("sizeof(x[j].d os %lu and sizeof(int) is %lu\n", sizeof(x[j].d), sizeof(int));

	for (int i=0 ; i < sizeof(x[j].d)/sizeof(int); i++)
		printf("At index %u value is %i\n",i,x[j].d[i]);
    }
}
