/*
  Swimming
 
  Count pool laps
 
 The circuit: 
 
 Created May 2011
 By Alec Clews
*/

//#include <WProgram.h>
#include <stdio.h>
#include <stdlib.h>

void startRecording(void){
	return;
};

typedef struct {
	char *entryText;
	char *entryDesc;
	struct menuType *nextMenu;
	void *callFunction(void);
} menuEntryType;

typedef struct menuType {
	char *menuName;
	char *menuDesc;
	menuEntryType menuEntries[3];
} menuType;

menuType setupMenu;

static const menuType const homeMenu = {
	"Home",
	"Home Menu",
	{
		"Setup",
		"Setup",
		&setupMenu,
		NULL,
	},
	{
		"Recording",
		"Recording",
		NULL,
		&startRecording,
	},
};

int processMenu(menuType *currentMenu)
{
	return 0;
}
	

// the loop() method runs over and over again,
// as long as the Arduino has power
void  loop(void)
{
	processMenu(&homeMenu);
}

// The setup() method runs once, when the program starts
void setup(void)
{                
}

