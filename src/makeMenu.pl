#!/usr/bin/perl 
#===============================================================================
#
#         FILE:  makeMenu.pl
#
#        USAGE:  ./makeMenu.pl  
#
#  DESCRIPTION:  Generate C code for Arduino Swimmer
#
#      OPTIONS:  ---
# REQUIREMENTS:  ---
#         BUGS:  ---
#        NOTES:  ---
#       AUTHOR:  YOUR NAME (), 
#      COMPANY:  
#      VERSION:  1.0
#      CREATED:  15/08/11 08:39:52
#     REVISION:  ---
#===============================================================================

use strict;
use warnings;


ue YAML::Any;

my $headerTemplate = <<<

typedef struct {
	char *entryText;
	char *entryDesc;
	struct menuType *nextMenu;
	void *callFunction(void);
} menuEntryType;

typedef struct menuType {
	char *menuName;
	char *menuDesc;
	menuEntryType menuEntries[3];
} menuType;

menuType setupMenu;

menuType homeMenu = {
	"Home",
	"Home Menu",
	{
		"Setup",
		"Setup",
		&setupMenu,
		NULL,
	},
	{
		"Recording",
		"Recording",
		NULL,
		&startRecording,
	},
};



